<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="text-align: center; width: 50%; margin: auto; border-radius: 10px; border: 1px solid black; padding: 10px;">
        <h2>Form search document</h2>
        <h4>Thêm độ ảnh hưởng của lượt view document lên kết quả tìm kiếm</h4>
        <p>
            score new = score cũ + number view
        </p>

        <form action="{{route('document.search')}}">
            <input type="text" value="{{ old('keyword') }}" name="keyword">
            <button type="submit">Search</button>
        </form>
    </div>

    <div style="padding: 10px; margin: auto; margin-top: 30px; border-top: 1px solid black;">
        <h3 style="text-align: center;">
            @if(isset($total) && $total > 0) {{$total}} @endif
            Result
        </h3>
        <div>
            @if(isset($data))

            @foreach ($data as $doc)

            <a href="{{route("document.detail", $doc['_source']['doc_id'])}}">
                <p> _score : {{ $doc['_score'] }} </p>
            </a>
            @foreach ($doc['_source'] as $key => $value)

            <span style="margin-left: 20px;"> {{ $key }} : {{ $value}} </span> <br/>

            @endforeach

            @endforeach

            @else
            <p>No results found</p>
            @endif
        </div>
    </div>

</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="padding: 10px;">
        <h2>Document detail</h2>
        <p> 
        <span style="font-weight : bold"> Id : </span> {{$document->doc_id}}</p>
        <p> 
        <span style="font-weight : bold"> Document name : </span> {{$document->doc_name}}</p>
        <p> 
        <span style="font-weight : bold"> Document name no access : </span> {{$document->doc_name_no_accent}}</p>
        <p> 
        <span style="font-weight : bold"> Document tag : </span> {{$document->doc_tag}}</p>
       
    </div>

     <div style="padding: 10px; margin: auto; margin-top: 30px; border-top: 1px solid black;">
        <h3 style="text-align: center;">
            Document related
        </h3>
        <div>
            @if(isset($data))

            @foreach ($data as $doc)

            <a href="{{route("document.detail", $doc['_source']['doc_id'])}}">
                <p> _score : {{ $doc['_score'] }} </p>
            </a>
            @foreach ($doc['_source'] as $key => $value)

            <span style="margin-left: 20px;"> {{ $key }} : {{ $value}} </span> <br/>

            @endforeach

            @endforeach

            @else
            <p>No results found</p>
            @endif
        </div>
    </div>

</body>

</html>
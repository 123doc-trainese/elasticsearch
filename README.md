# Tổng quan 
- Elasticsearch thực chất là một server chạy trên nền tàng Apache Lucene 
-> Ví dụ so sánh về động cơ và ô tô

- Các thuật ngữ thường hay nhắc đến 
    +  relevance (độ liên quan)
    +  index (tương đương với database trong mysql)
    +  type (tương đương với table trong mysql)
    +  document (tương ứng với record trong mysql)
    +  term (từ, từ khóa)
    +  field (có thể gọi là trường - field)

# Index
- forward index => Giống với phần mục lục trong sách (page -> words)
- inverted index => Giống với phần phụ lục của cuốn từ điển (words -> pages)

=> Thay vì đọc từng page để tìm kiếm, ES sẽ tìm kiếm keyword trong index nên kết quả trả về sẽ rất nhanh.

# Score 
- Thuật toán 'phổ biến' đánh giá điểm trên ELS : term frequency/inverse document frequency => TF/IDF
    * term frequency => đánh giá tần suất xuất hiện của term trong field
        + Ví dụ : Search từ khóa quick trong 2 title sau

        ```js
        { "title": "The quick brown fox jumps over the quick dog" }

        { "title": "The quick brown fox" }
        ```
        + => tf(t, d) = √frequency (frequency : Số lần xuất hiện của từ khóa trong document)

    * Inverse document frequency => đánh giá tần suất xuất hiện của term trên toàn bộ index (càng xuất hiện nhiều, càng ít thích hợp)
        + vd : search 2 từ khóa "công ty" => (170 tr kết quả) và "Công ty VNP Group" => (3 triệu) trên google
        + => idf(t) = 1 + log ( numDocs / (docFreq + 1)) 
        + numDocs : Tổng số document trong index
        + docFreq : Số documents xuất hiện từ khóa

    * Field-length norm => đánh giá độ dài của field
        + Field càng ngắn, thì term sẽ có giá trị càng cao; và ngược lại. (key xuất hiện trong title sẽ có giá trị cao hơn khi xuất hiện trong content)
        + => norm(d) = 1 / √numTerms

    * => _score  = IDF * TF * fieldNorms

- Thuật toán BM25 từ bản  > 5.0 
    * idf(t) = log(1 + (docCount - docFreq + 0.5) / (docFreq + 0.5))
    * tf(t) = ((k + 1) * freq) / (k + freq) : k là hằng số  , ELS đang quy định là 1.2

    * => Công thức BM25 : IDF * (freq * (k1 + 1)) / (freq + k1 * (1 - b + b * (fieldLength / avgFieldLength)))

# Customer score
- ELS cung cấp cho người dùng function-score để customer được score trả ra . Dưới đây là các thông số mà func cung cấp : 
    + score_mode : Cách tính điểm trả ra của score mặc định , score người dùng định nghĩa (nhân, cộng , trung bình cộng, max và min)
    + boost_mode : định nghĩa cách tính điểm khi mà có các điểm mới được tạo ra bằng function score, có 6 kiểu, nhân, cộng, trung bình cộng, thay thế, min, max
    + boost : điểm để tăng dùng để tính điểm cho các kết quả truy vấn bằng query ( query trong function_score)
    + functions : tập hợp các function để tính thêm với các index đã được chọn và tính điểm bằng “query”

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document', function (Blueprint $table) {
            $table->integer('doc_id');
            $table->text('doc_type');
            $table->integer('doc_type_id');
            $table->text('doc_code');
            $table->text('doc_path_extract');
            $table->string('doc_name');
            $table->string('doc_name_no_accent');
            $table->string('doc_name_md5');
            $table->string('doc_filename');
            $table->text('doc_path');
            $table->text('doc_filesize');
            $table->text('doc_pdf_full_filesize');
            $table->text('doc_txt_full_filesize');
            $table->integer('doc_ext_id');
            $table->text('doc_css');
            $table->integer('doc_category_id');
            $table->integer('doc_user_id');
            $table->text('doc_user_ip');
            $table->text('doc_date_create');
            $table->text('doc_last_update');
            $table->text('doc_tag');
            $table->text('doc_limit');
            $table->text('doc_is_convert');
            $table->text('doc_convert_status');
            $table->text('doc_outline');
            $table->text('doc_convert_error');
            $table->text('doc_convert_version');
            $table->text('doc_is_move');
            $table->text('doc_approval');
            $table->text('doc_is_password');
            $table->text('doc_status');
            $table->text('doc_active');
            $table->text('doc_info_status');
            $table->text('doc_admin_active');
            $table->text('doc_admin_active_date');
            $table->text('doc_num_page');
            $table->text('doc_avatar');
            $table->text('doc_is_sale');
            $table->text('doc_num_page_demo');
            $table->text('doc_percent_page_demo');
            $table->text('doc_money_sale');
            $table->integer('doc_tag_id');
            $table->integer('doc_user_tag_id');
            $table->text('doc_is_tag');
            $table->text('doc_top');
            $table->text('doc_top_order');
            $table->text('doc_future_order');
            $table->text('doc_future');
            $table->text('doc_views');
            $table->text('doc_downloads');
            $table->text('doc_likes');
            $table->text('doc_rate');
            $table->text('doc_views_of_time');
            $table->text('doc_downloads_of_time');
            $table->text('doc_language');
            $table->text('doc_num_share');
            $table->text('doc_fb_share');
            $table->text('doc_count_comments');
            $table->integer('doc_root_id');
            $table->text('doc_root');
            $table->text('doc_is_cron');
            $table->text('doc_is_search_title');
            $table->text('doc_is_text');
            $table->text('doc_is_delete');
            $table->text('doc_is_cron_text');
            $table->text('doc_is_cron_seo');
            $table->text('doc_is_cron_wordpress');
            $table->text('doc_is_cron_copy');
            $table->integer('lang_id');
            $table->text('doc_is_check_duplicate');
            $table->integer('doc_file_attach_id');
            $table->text('doc_deny');
            $table->integer('doc_event_id');
            $table->text('doc_event_status');
            $table->integer('doc_test_id');
            $table->integer('doc_test_id_2');
            $table->integer('doc_list_relative_id');
            $table->text('doc_list_relative_id_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
};

<?php

use App\Http\Controllers\DocumentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['as' => 'document.', 'prefix' => 'document'], function () {
    Route::get('/', [DocumentController::class, 'index'])->name('index');
    Route::get('/search', [DocumentController::class, 'search'])->name('search');
    Route::get('/detail/{id}', [DocumentController::class, 'detail'])->name('detail');
});

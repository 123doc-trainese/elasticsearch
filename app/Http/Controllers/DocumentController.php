<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Services\ElasticsearchService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DocumentController extends Controller
{
    private const INDEX = "document,categories_multi";

    public function index()
    {
        return view('document');
    }

    public function detail($id)
    {
        $document = Document::where('doc_id', $id)->first();

        $docRelated = $this->docRelated($document->doc_name, 20, $document->doc_category_id);

        return view('detail', [
            'document' => $document,
            'data' => $docRelated['result'],
        ]);
    }

    public function search(Request $request)
    {
        $data = [];
        $count = 0;

        if (!is_null($request->keyword)) {
            $response = $this->searchByKeyword($request->keyword);

            $data = $response['result'];
            $count = $response['count'];
        }

        return view('document', [
            'data' => count($data) == 0 ? null : collect($data),
            'total' => $count,
        ]);
    }

    private function docRelated($text, $size, $categoryId)
    {
        $should = [];
        $es = new ElasticsearchService();

        $response = $es->getAnalyze(
            'index-customer-analyzer',
            ['analyzer' => 'document_vi_analyzer', 'text' => $text]
        );

        foreach ($response['tokens'] as $key) {
            Log::debug($key['token']);
            array_push(
                $should,
                ['match' => ['doc_name' => $key['token']]],
                ['match' => ['doc_name_no_accent' => $key['token']]]

            );
        }

        $query = [
            'size' => $size,
            '_source' => [
                "doc_id",
                'doc_path_extract',
                'doc_name',
                'doc_name_no_accent',
                'doc_tag'
            ],
            'query' => [
                "function_score" => [
                    'query' => [
                        'bool' => [
                            'should' => $should,
                            'must' => [
                                'match' => ['doc_category_id' => $categoryId]
                            ]
                        ],
                    ],
                    'script_score' => [
                        'script' => "_score + Math.log10(doc['doc_views'].value + 1)"
                    ],
                    'score_mode' => 'sum',
                    'boost_mode' => 'sum'
                ],
            ],
        ];

        return $this->mapResponse($this->searchELS(self::INDEX, $query));
    }

    private function searchByKeyword($keyword, $size = 20)
    {
        $query = [
            'indices_boost' => [
                ['categories_multi' => 3],
                ['document' => 1.5],
            ],
            'size' => $size,
            '_source' => [
                "doc_id",
                'doc_path_extract',
                'doc_name',
                'doc_name_no_accent',
                'doc_tag',
                'doc_category_id',
            ],
            'query' => [
                "function_score" => [
                    'query' => [
                        'bool' => [
                            'should' => [
                                [
                                    'match' => [
                                        'doc_name' => $keyword
                                    ]
                                ],
                                [
                                    'match' => [
                                        'doc_name_no_accent' => $keyword
                                    ]
                                ],
                                [
                                    'match' => [
                                        'cat_name' => $keyword
                                    ]
                                ],
                            ],
                        ],
                    ],
                    'script_score' => [
                        'script' => "_score + Math.log10(doc['doc_views'].value + 1)"
                    ],
                    'score_mode' => 'sum',
                    'boost_mode' => 'sum'
                ],
            ],
        ];

        return $this->mapResponse($this->searchELS(self::INDEX, $query));
    }

    private function searchELS($index, $query)
    {
        $es = new ElasticsearchService();
        return $es->search($index, $query);
    }

    private function mapResponse($response)
    {
        $result = isset($response['hits']['hits']) ? $response['hits']['hits'] : null;
        $count = isset($response['hits']['total']) ? $response['hits']['total']['value'] : null;

        return [
            'result' => $result,
            'count' => $count,
        ];
    }
}

<?php

namespace App\Services;

use Carbon\Carbon;
// use Elastic\Elasticsearch\ClientBuilder;

use Elasticsearch\ClientBuilder;

class ElasticsearchService
{
    private $client;

    public function __construct()
    {
        $this->client = ClientBuilder::create()
            ->setHosts([config('elasticsearch.host')])
            ->build();
    }

    public function index($index, $body)
    {
        $params = [
            'index' => $index,
            'body'  => $body
        ];

        $result = $this->client->index($params);

        return $result;
    }

    public function update($index, $id, $body)
    {
        $body['modified'] = Carbon::now()->timestamp;

        $params = [
            'id' => $id,
            'index' => $index,
            'body' => [
                'doc' => $body,
            ]
        ];

        $result = $this->client->update($params);

        return $result;
        // return $result->asArray(); // for version > 8.0
    }

    public function search($index, $body)
    {
        $params = [
            'index' => $index,
            'body' => $body
        ];

        $result = $this->client->search($params);

        return $result;
        // return $result->asArray();  // for version > 8.0
    }

    public function initIndex($name)
    {
        $params = [
            'index' => $name
        ];

        $result = $this->client->indices()->create($params);

        return $result;
        // return $result->asArray(); // for version > 8.0
    }

    public function getAnalyze($index, $body)
    {
        $params = [
            'index' => $index,
            'body' => $body,
        ];

        $result = $this->client->indices()->analyze($params);

        return $result;
    }

    public function settingAnalyzer($index)
    {
        $params = [
            'index' => $index,
            'body' => [
                'settings' => [
                    'analysis' => [
                        'analyzer' => [
                            'document_vi_analyzer' => [
                                'type' => 'vi_analyzer',
                                'split_url' => 'false',
                                'keep_punctuation' => 'true',
                                'stopwords' => [],
                            ]
                        ]
                    ],
                ]
            ]
        ];

        $response = $this->client->indices()->create($params);

        dd($response);
    }
}

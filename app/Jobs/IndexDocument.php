<?php

namespace App\Jobs;

use App\Models\Document;
use App\Services\ElasticsearchService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IndexDocument implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private const INDEX_DOCUMENT = "document";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $es = new ElasticsearchService();

        $documents = $this->getDocument();
        foreach ($documents as $doc) {
            $es->index(self::INDEX_DOCUMENT, $doc);
            echo "=== [Index for document {$doc['doc_id']} success] ===\n";
        }
    }

    /**
     * Get data document
     * @return $document
     */
    private function getDocument()
    {
        $documents = Document::get()->chunk(100);

        return $documents->toArray();
    }
}

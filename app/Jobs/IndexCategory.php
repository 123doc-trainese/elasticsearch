<?php

namespace App\Jobs;

use App\Models\Category;
use App\Services\ElasticsearchService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IndexCategory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private const INDEX_CATEGORY = "categories_multi";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $es = new ElasticsearchService();

        $category = $this->getCategory();
        foreach ($category as $doc) {
            $es->index(self::INDEX_CATEGORY, $doc);
            echo "=== [Index for category {$doc['cat_id']} success] ===\n";
        }
    }

    /**
     * Get data document
     * @return $document
     */
    private function getCategory()
    {
        $documents = Category::get()->chunk(100);

        return $documents->toArray();
    }
}

<?php

namespace App\Jobs;

use App\Models\DocumentText;
use App\Services\ElasticsearchService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IndexDocumentText implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private const INDEX_DOCUMENT_TEXT = "document_text_1";

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $es = new ElasticsearchService();

        $documentText = $this->getDocumentText();

        foreach ($documentText as $doc) {
            $es->index(self::INDEX_DOCUMENT_TEXT, $doc);
            echo "=== [Index for document text {$doc['doct_id']} success] ===\n";
        }
    }

    /**
     * Get data document
     * @return $document
     */
    private function getDocumentText()
    {
        $documents = DocumentText::get()->chunk(100);

        return $documents->toArray();
    }
}

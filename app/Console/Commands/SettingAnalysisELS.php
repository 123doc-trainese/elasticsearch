<?php

namespace App\Console\Commands;

use App\Services\ElasticsearchService;
use Illuminate\Console\Command;

class SettingAnalysisELS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setting:analysis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setting analyzer for system ELS';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $index = 'index-customer-analyzer';
        $es = new ElasticsearchService();
        $es->settingAnalyzer($index);
    }
}

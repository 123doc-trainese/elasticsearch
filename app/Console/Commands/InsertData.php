<?php

namespace App\Console\Commands;

use App\Jobs\IndexCategory;
use App\Jobs\IndexDocumentText;
use App\Jobs\IndexDocument;
use App\Models\Document;
use Illuminate\Console\Command;

class InsertData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'insert:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert data for index document';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        dispatch(new IndexDocument());
        dispatch(new IndexDocumentText());
        dispatch(new IndexCategory());

        return Command::SUCCESS;
    }
}

<?php

namespace App\Console\Commands;

use App\Services\ElasticsearchService;
use Elastic\Elasticsearch\Response\Elasticsearch;
use Illuminate\Console\Command;

class IndexDocument extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create index elasticsearch';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $es = new ElasticsearchService();

        $es->initIndex(config('elasticsearch.prefix') . 'document');
        echo "=== Index document success ===\n";

        $es->initIndex(config('elasticsearch.prefix') . 'categories_multi');
        echo "=== Index categories_multi success ===\n";

        $es->initIndex(config('elasticsearch.prefix') . 'document_text_1');
        echo "=== Index document_text_1 success ===\n";

        return Command::SUCCESS;
    }
}

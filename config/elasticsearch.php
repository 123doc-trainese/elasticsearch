<?php

return [
    'host' => env('ELASTICSEARCH_HOST', 'http://elasticsearch:9200'),
    'prefix' => env('ELASTICSEARCH_PREFIX', '')
];
